﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyBlog.Models
{
    public class MutipleModel
    {
        public PostModel Post { get; set; }
        public CommentModel Comment { get; set; }
    }
}
