﻿
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyBlog.Models
{
    public class PostInputModel
    {
        public int ID { get; set; }
        public string Username { get; set; }
        public string Title { get; set; }
        public IFormFile Image { get; set; }
        public string Genre { get; set; }
        public DateTime Date { get; set; }
        public string Summary { get; set; }
        public string Content { get; set; }

        public List<CommentModel> Comments { get; set; }

        public PostInputModel(  )
        {
        }

        public PostInputModel(string username, string title, IFormFile image, string genre, DateTime date, string summary, string content, List<CommentModel> comments)
        {
            Username = username;
            Title = title;
            Image = image;
            Genre = genre;
            Date = date;
            Summary = summary;
            Content = content;
            Comments = comments;
        }

        public PostInputModel(string username, string title, IFormFile image, string genre, DateTime date, string summary, string content)
        {
            Username = username;
            Title = title;
            Image = image;
            Genre = genre;
            Date = date;
            Summary = summary;
            Content = content;
            Comments = new List<CommentModel>();
        }
    }
}
