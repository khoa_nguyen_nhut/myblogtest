import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Post } from 'src/app/model/Post';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  private apiUrl = 'https://localhost:44308/api/';
  constructor(private http: HttpClient) { }
  public  getPosts(): Observable<Post[]>{
    return this.http.get<Post[]>(this.apiUrl + 'Post').pipe(
      catchError(this.errorHandler)
    );
  }

  public getPost(id: number): Observable<Post>{
    return this.http.get<Post>(this.apiUrl + 'Post/' + id ).pipe(
      catchError(this.errorHandler)
    );
  }

  errorHandler(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
 }
}

