import { PostService } from './../post.service';
import { Post } from 'src/app/model/Post';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-blog-post-detail',
  templateUrl: './blog-post-detail.component.html',
  styleUrls: ['./blog-post-detail.component.css']
})
export class BlogPostDetailComponent implements OnInit {
  post: Post;
  constructor(
    private route: ActivatedRoute,
    private postservice: PostService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.postservice.getPost(+parseInt(params.get('postid'), 10)).subscribe(result =>
        this.post = result);
    });
  }

}
