import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class AccountService {
  private user: User;
  constructor() {
  }
  public login(http: HttpClient, user: User){
    http.post<User>('https://localhost:44308/' + 'Account', user).subscribe(result => {
      this.user = result;
    }, error => console.error(error));
  }

}
interface  User {
  username: string;
  password: string;
}
