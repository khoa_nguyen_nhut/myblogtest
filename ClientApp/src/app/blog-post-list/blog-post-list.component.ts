import { PostService } from '../post.service';
import { Component, OnInit } from '@angular/core';
import { Post } from 'src/app/model/Post';

@Component({
  selector: 'app-blog-post-list',
  templateUrl: './blog-post-list.component.html',
  styleUrls: ['./blog-post-list.component.css']
})
export class BlogPostListComponent implements OnInit {
  postList: Post[];

  constructor(private postService: PostService) { }

  ngOnInit(): void {
   this.postService.getPosts().subscribe(result => {
     this.postList = result;
   });
  }

}
