export interface Comment {
  id: string;
  username: string;
  email: string;
  date: Date;
  commentContent: string;
}
