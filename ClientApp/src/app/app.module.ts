import { PostService } from './post.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { BottomBarComponent } from './bottom-bar/bottom-bar.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { HttpClientModule } from '@angular/common/http';
import { BlogPostListComponent } from './blog-post-list/blog-post-list.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { AboutMeComponent } from './about-me/about-me.component';
import { BlogPostDetailComponent } from './blog-post-detail/blog-post-detail.component';
import { RegisterFormComponent } from './register-form/register-form.component';

@NgModule({
  declarations: [
    AppComponent,
    TopBarComponent,
    BottomBarComponent,
    FetchDataComponent,
    BlogPostListComponent,
    LoginFormComponent,
    AboutMeComponent,
    BlogPostDetailComponent,
    RegisterFormComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [PostService],
  bootstrap: [AppComponent]
})
export class AppModule { }
